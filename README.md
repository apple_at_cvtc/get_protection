Downloads the latest GlobalProtect pkg from the CVTC gateway and holds it.

Find the pkg at https://bitbucket.org/apple_at_cvtc/get_protection/downloads/

If the build starts failing for some reason, you probably need to replace the `BB_AUTH_STRING` variable in Settings > Variables. It is a string in the format `username:token` where token is a BitBucket auth token with the `Write` permission to all repositories. See https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html